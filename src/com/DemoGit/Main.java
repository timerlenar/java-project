/*ООО "Круче Гугла" наняло трёх разработчиков. Внезапно разработчики узнали, что их зарплаты различаются. Разработчики решили объявить забастовку, если разница максимальной и минимальной зарплаты превысит определённый уровень. Определите, грозит ли ООО "Круче Гугла" забастовка.

        Формат ввода:

        В первой строке - зарплаты разработчиков через пробел, три целых числа.

        Во второй строке - разница, при превышении которой будет объявлена забастовка.

        Формат вывода:

        "Ура, бастуем!" - если критический уровень превышен;

        "За работу, Солнце ещё высоко" - если критический уровень не превышен.
*/
import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Scanner;

import static java.lang.Integer.parseInt;
import static java.lang.Math.sqrt;

/*class Mammin {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        int d = sc.nextInt();
        if (a == b & b == c) {
            System.out.println("За работу, Солнце ещё высоко");}
        else if (a >= b & b >= c) {
            if (a - c > d) {
                System.out.println("Ура, бастуем!");}
            else {
                System.out.println("За работу, Солнце ещё высоко");}}
        else if (a >= c & c >= b) {
            if (a - b > d) {
                System.out.println("Ура, бастуем!");}
            else {
                System.out.println("За работу, Солнце ещё высоко");}}
        else if (b >= a & a >= c) {
            if (b - c > d) {
                System.out.println("Ура, бастуем!");}
            else {
                System.out.println("За работу, Солнце ещё высоко");}}
        else if (b >= a & c >= a & b >= c) {
            if (b - a > d) {
                System.out.println("Ура, бастуем!");}
            else {
                System.out.println("За работу, Солнце ещё высоко");}}
        else if (c >= b & b >= a) {
            if (c - a > d) {
                System.out.println("Ура, бастуем!");}
            else {
                System.out.println("За работу, Солнце ещё высоко");}}
        else if (c >= b & b <= a & c >= a) {
            if (c - b > d) {
                System.out.println("Ура, бастуем!");}
            else {
                System.out.println("За работу, Солнце ещё высоко");}}
    }
}
*/

/*На числовой прямой даны два отрезка, заданных парами целых чисел: [a1, b1], [a2, b2].
 Напишите программу, которая находит их пересечение.
Если пересечение - отрезок, необходимо вывести два числа (границы отрезка)
, если единственная точка - единственное число (точку),
 если пересечения нет - вывести фразу "Пересечения нет" (без кавычек).
 */


/*class Otrezok {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a1 = sc.nextInt();
        int b1 = sc.nextInt();
        int a2 = sc.nextInt();
        int b2 = sc.nextInt();
        //1
        if (a1 < b1 & b1 < a2 & a2 < b2) {
            System.out.println("Пересечения нет");}
        //2
        else if (a2 < b2 & b2 < a1 & a1 < b1) {
            System.out.println("Пересечения нет");}
        //3
        else if (a1 < a2 & a2 < b2 & b2 < b1) {
            System.out.println(a2+" "+b2);}
        //4
        else if (a2 < a1 & a1 < b1 & b1 < b2) {
            System.out.println(a1+" "+b1);}
        //5
        else if (a1 < a2 & a2 < b1 & b1 < b2) {
            System.out.println(a2+" "+b1);}
        //6
        else if (a2 < a1 & a1 < b2 & b2 < b1) {
            System.out.println(a1+" "+b2);}
        //7
        else if (a1 < b1 & b1 == a2 & a2 < b2) {
            System.out.println(b1);}
        //8
        else if (a2 < b2 & b2 == a1 & b2 < b1) {
            System.out.println(b2);}
        //9
        else if (a1 == a2 & a1 < b1 & b1 == b2) {
            System.out.println(a1+" "+b1);}
        //10
        else if (a1 == a2 & a2 < b1 & b1 < b2) {
            System.out.println(a2+" "+b1);}
        //11
        else if (a1 < a2 & a2 < b1 & b1 == b2) {
            System.out.println(a2+" "+b1);}
        //12
        else if (a2 < a1 & a1 < b2 & b1 == b2) {
            System.out.println(a1+" "+b1);}
        //13
        else if (a1 == a2 & a2 < b2 & b2 < b1) {
            System.out.println(a2+" "+b2);}
    }
}
*/

/*На вход подаётся три целых числа. Выведите максимальное чётное из них.
 Если чётных чисел нет, выведите "Чётных чисел нет".
 */

/*class maxeven {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        if (a % 2 != 0 & b % 2 != 0 & c % 2 != 0) {
            System.out.print("Чётных чисел нет");
        }
        else if (a % 2 == 0 & b%2!=0 & c%2!=0)    {
            System.out.println (a);
        }
        else if (a % 2 != 0 & b%2==0 & c%2!=0)    {
            System.out.println (b);
        }
        else if (a % 2 != 0 & b%2!=0 & c%2==0)    {
            System.out.println (c);}
        else if (a % 2 == 0 & b%2==0 & c%2!=0)    {
            if(a>=b){
                System.out.println (a);
            }
            else {System.out.println (b);}
        }
        else if (a % 2 != 0 & b%2==0 & c%2==0)    {
            if(b>=c){
                System.out.println (b);
            }
            else {System.out.println (c);}
        }
        else if (a % 2 == 0 & b%2!=0 & c%2==0)    {
            if(a>=c){
                System.out.println (a);
            }
            else {System.out.println (c);}
        }
        else if (a%2==0 & b%2==0 & c%2==0){
            if (a<b & b<c)    {
                System.out.println (c);
            }
            else if (a>b & b>c)    {
                System.out.println (a);
            }
            else if (b>a & a>c)    {
                System.out.println (b);
            }
            else if (c>a & a>b)    {
                System.out.println (c);
            }
            else if (a==b & b>c)    {
                System.out.println (a);
            }
            else if (a==c & c>b)    {
                System.out.println (a);
            }
            else if (b==c & c>a)    {
                System.out.println (b);
            }
            else if (a==b & b==c)    {
                System.out.println (a);
            }

        }

    }
}

*/


class Sravnenie {
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        String s = sc.nextLine();

        switch (s) {
            case "System.out.println()":
                System.out.println("Это команда вывода на печать");
                break;
            case "if":
                System.out.println("Это условный оператор");
                break;
            case "else":
                System.out.println("Это альтернативная конструкция условного оператора");
                break;
            default:
                System.out.println("Раздел в разработке");
        }

    }
}



class trexznach {
    public static void main(String[] args) {
        Scanner sc = new Scanner (System.in);
        int x = sc.nextInt();
        if (x > 0 & x/100>=1 & x/100<10 ) {
            System.out.println("YES");}
        else {
            System.out.println("NO");
        }
    }
}


class iscontain {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine().toLowerCase();
        String b = sc.nextLine().toLowerCase();
        boolean c = a.contains(b);
        System.out.println(c);
    }
}


/*На вход подаётся целое трёхзначное число, а затем цифра.
Выведите true, если эта цифра является средней в числе (разряд десятков),
 и false - если нет. Если введённое число не является трёхзначным, выведите "error".
 */

class sredneechislo {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        a = Math.abs(a);
        b = Math.abs(b);
        if (a / 100 >= 1 & a/100 <10) {
            if ((a / 10) % 10 == b) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }}

        else{
            System.out.println("error");
        }
    }
}


/*На вход подаётся два числа - a и b. Выведите знак отношения между числами:
 один символ "<", если a < b, ">", если a > b и "=", если a=b.
 */
class bolshemensheravno {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        if (a<b) {
            System.out.println("<");
        }
        else if (a>b) {
            System.out.println(">");
        }
        else if (a==b)  {
            System.out.println("=");
        }

    }
}


/*Выведите время года по введённому номеру месяца.
Если введён ошибочный номер месяца, выведите "error".
 */
class vremenagoda {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        switch (a) {
            case 12: case 1: case 2:
                System.out.println("Зима");
                break;
            case 3: case 4: case 5:
                System.out.println("Весна");
                break;
            case 6: case 7: case 8:
                System.out.println("Лето");
                break;
            case 9: case 10: case 11:
                System.out.println("Осень");
                break;
            default:
                System.out.println("error");
                break;
        }
    }
}


 /*   На вход подаются координаты точки x, y.
 Определите, попадает ли точка в заштрихованную область.
 y=2-x^2 ; y=x;
  */

class grafik {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        float x = sc.nextFloat();
        float y = sc.nextFloat();
        if (x >= 0 & y <= (2 - x * x) & y>=0) {
            System.out.println("Yes");
        }
        else if (x < 0 & y >= x & y <= (2 - x * x)) {
            System.out.println("Yes");}
        else {
            System.out.println("No");
        }
    }
}




  /*  На вход подаётся слово. Выведите словами количество букв в этом слове.
  Если букв больше пяти - выведите "Длинное слово".
   */

class mnogobukv {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine();
        int b = a.length();
        switch (b) {
            case 1:
                System.out.println("Одна буква");
                break;
            case 2:
                System.out.println("Две буквы");
                break;
            case 3:
                System.out.println("Три буквы");
                break;
            case 4:
                System.out.println("Четыре буквы");
                break;
            case 5:
                System.out.println("Пять букв");
                break;
            default:
                System.out.println("Длинное слово");
        }
    }
}

/*Богатейшие люди Земли решили создать тайное мировое правительство
  ̶и̶ ̶у̶п̶р̶а̶в̶л̶я̶т̶ь̶ ̶п̶л̶а̶н̶е̶т̶о̶й̶,̶ ̶н̶е̶ ̶
  п̶р̶и̶в̶л̶е̶к̶а̶я̶ ̶в̶н̶и̶м̶а̶н̶и̶я̶ ̶с̶а̶н̶и̶т̶а̶р̶о̶в̶.
  В кабинет совещаний могут войти только те, кто указан в специальном списке:
   Джефф Безос, Илон Маск,  Марк Цукерберг, Билл Гейтс.
   Чтобы получить допуск, нужно сказать фразу-приветствие.
    Если фраза-приветствие содержит имя из списка, проход разрешается.
    Если же нет - проход блокируется.
 На ввод подаётся фраза-приветствие. Выведите "Добро пожаловать!",
 если имя есть в списке, и "Здесь никого нет, Вы ошиблись дверью" - если нет.
 Примечание. Буква "ё".
*/


class bogachi {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String a = sc.nextLine();
        String b = "Джефф Безос";
        String c = "Илон Маск";
        String d = "Марк Цукерберг";
        String e = "Билл Гейтс";
        if (a.contains(b) == true || a.contains(c) == true || a.contains(d) == true || a.contains(e) == true) {
            System.out.println("Добро пожаловать!");}
        else { System.out.println("Здесь никого нет, Вы ошиблись дверью");}

    }
}


class MyNumber {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int x = 1;
        while (x<=a){
            System.out.println (x);
            x++;}

    }
}

/*На ввод подаётся натуральное число n.
Выведите на печать в возрастающем порядке через пробел квадраты натуральных чисел,
 если эти квадраты не превышают n.
 */

class quad {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int x = 1;
        while (x*x<=a){
            System.out.print (x*x + " ");
            x++;}

    }
}


    /* Считайте со ввода последовательность целых чисел
    . Последовательность оканчивается числом 0.
     Выведите на печать сумму введённых чисел.*/

class sumchisel {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = 0;
        int sum=0;
        while ((a=sc.nextInt())!= 0) {
            sum = sum+a; }
        System.out.print (sum);

    }
}


/*На вход подаётся последовательность слов.
 Посчитайте общее количество введённых слов.

Примечание. Так как маркер конца последовательности в этой задаче отсутствует,
 будет удобно воспользоваться методом hasNext().
Найдите самостоятельно, как он работает.*/



class schetchikslov {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int x = 0;
        while(sc.hasNext()==true){
            String s = sc.next();
            x++;
        }
        System.out.println(x);

    }
}
//чтобы прекратить ввод слов необходимо ввести ctrl+d


/*На вход подаются числа, которые делятся на 11.
Концом последовательности будет любое число, не делящееся на 11
 (это число не входит в последовательность).
  Посчитайте количество введённых чисел и сумму тех из них, которые кратны 3.*/

class kratno11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = 0;
        int kol=0;
        int sum =0;
        while ((a=sc.nextInt())%11==0) {
            kol++;
            if (a%3==0) {
                sum+=a;}
        }
        System.out.println (kol + "\n" + sum);

    }
}


/*На вход подаётся последовательность чисел от нуля до 10,
являющихся рейтингами фильма, выставленными зрителями.
Если входящее число отрицательное или больше 10, последовательность прерывается.
Посчитайте среднюю оценку фильма. */

class Mn {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        double q;
        int n = 0;
        int v = 0;
        int w = 0;
        do {
            n = sc.nextInt();
            if(n <= 10 && n >= 0){
                w = n + w;
                v++;
            }
        }
        while(sc.hasNext());

        System.out.println((double)w/(double)v);

    }
}


/*На вход подаётся натуральное число.
Выведите на печать составляющие его цифры в обратном порядке.
 */

class obratno {
    public static void main(String[] args){
        Scanner sc = new Scanner (System.in);
        long x = sc.nextLong();
        String y = Long.toString(x);
        int z = y.length()-1;
        while (z>=0) {
            System.out.print(y.charAt(z));
            z-=1;
        }
    }
}



  /*  На вход подаётся строка с двумя буквами, а затем - строка, состоящая из слов.
  Выведите на печать все слова, которые начинаются на буквы,
  идущие по алфавиту между буквами с первой строки (включительно).
  Вывести слова необходимо в том же порядке, в котором они подаются на вход.
   */

class MyStudy {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String letter = sc.nextLine();
        while (sc.hasNext()) {
            String s = sc.next();
            if (s.charAt(0) >= letter.toLowerCase().charAt(0) && s.charAt(0) <= letter.toLowerCase().charAt(2)) {
                System.out.println(s);
            } else if (s.charAt(0) <= letter.toLowerCase().charAt(0) && s.charAt(0) >=letter.toLowerCase().charAt(2)) {
                System.out.println(s);
            }
        }
    }
}


/* На вход подаётся два натуральных числа x и y.
Выведите на печать прямоугольник из звёздочек размером x*y.
 */

class zvezda {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int y = sc.nextInt();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                System.out.print("* ");
            }
            System.out.println();
        }
    }
}

/*На вход подаётся несколько целых чисел.
Выведите эти числа, возведённые в степени от двух до пяти.
Формат вывода: для каждого введённого числа степени выводятся в отдельной строке
 через пробел.
 Примечание. Каждая строка вывода оканчивается числом, а не пробелом.*/


class stepen {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //double x = sc.nextDouble();
        while (sc.hasNext()){
            int x = sc.nextInt();

            System.out.print(x * x + " " + x * x * x + " " + x * x * x * x + " " + x * x * x * x * x+ "\n");

        }
    }
}




class treug {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        for (int i=1; i<=x; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
                if (j < i) {
                    System.out.print(" ");
                }}
            System.out.println();
        }

    }
}


class odindva {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int count = 0;
        for (int i=1; i<=x; i++) {
            for (int j = 1; j <= i; j++) {
                count++;
                if (count>x){
                    break;}

                System.out.print(i+" ");
            }


        }
    }
}


class lesenka {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        for (int i = 1; i <= num; i++) {
            int count = 1;
            for (int j = 1; j <= i; j++) {
                for (int k = 1; k <= j; k++) {
                    if (count <= i) {
                        System.out.print(j);
                    }
                    if (count < i) {
                        System.out.print(" ");
                    }
                    count++;
                }
            }
            if (i < num) {
                System.out.println("");
            }
        }
    }
}




/* Выведите ответ с точностью 5 знаков после запятой.
 Гарантируется, что выражение имеет действительное значение.
 Результат выведите в виде числа типа double.
 Примечание. Если в ответе получается число с количеством знаков
  после запятой меньше, чем 5, выводить дополнительные нули не нужно!
 */

class vozvedstepen {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int y = sc.nextInt();
        int n = sc.nextInt();
        double z = Math.pow(x, y);
        double k = Math.pow(z, 1.0/n);
        System.out.println(String.format("%."+5+"f", k));

    }
}


/* perimeter pryamoug treug*/
class teoremacosinusov {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        System.out.println((int ) Math.round((Math.sqrt  (b * b + a * a) + a + b))*1.0);
    }
}

/* На вход подаётся целое число. Выведите его максимальную цифру.*/
class maxcifra {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = Math.abs(sc.nextInt());
        int max = 0;
        while (n>0){
            int digit = n % 10;
            n = n / 10;
            if (max < digit)
                max = digit;
        }
        System.out.println(max);
    }
}



   /*Выведите все нечётные целые числа, находящиеся между
   введёнными числами (включительно),
   в одной строке через пробел, в порядке возрастания.
    */

class nechet {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        int y = sc.nextInt();
        if (x<y){
            for (int i=x; i<=y; i++ ) {
                if (i % 2!=0) {
                    System.out.print(i + " ");
                }}}
        if (x>y) {
            for (int i=y; i<=x; i++ ) {
                if (i % 2!=0) {
                    System.out.print(i + " ");
                }}}
    }
}


/*простое число*/
class prostoe {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = Math.abs(sc.nextInt());
        int count = 0;
        if (x % 2 ==0 || x%3==0 || x==1 || x==0) {
            System.out.print (false);}
        else {
            for (int i = 2; i <=x; i++) {
                if (x%i==0) {
                    count +=1;
                }}}
        if (count > 1 ) {
            System.out.println("false");}
        else {System.out.println("true");}
    }
}



class arr {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        int y = sc.nextInt();
        while (sc.hasNext()){ int x;
            for (int i=1; i<=n; i++){ x = sc.nextInt(); arr[i-1] = x; } }
        System.out.print(arr[y-1]); } }





class MyProgram {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];

        while (sc.hasNext()){ int x;
            for (int i=1; i<=n; i++){ x = sc.nextInt(); arr[i-1] = x; } }
        int y = sc.nextInt();
        System.out.println(arr[y]); } }




class MyProgr {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];

        for (int i=1; i<=n; i++){
            int x = sc.nextInt(); arr[i-1] = x;}
        for (int i=1; i<=arr.length-1; i++) {
            if (i>0 && arr[i] > arr[i-1]) {
                System.out.print(" "+arr[i]);
            }
        }


    } }


class MyNumbe {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int [] numbers = new int [n];
        for (int i = 1; sc.hasNext(); i++){
            numbers [i-1]=i;}
        Arrays.sort(numbers);
        /*if (k>=0 & k<numbers.length){
            System.out.println (numbers [k]);}
        else {
            System.out.println ("Ошибка ввода.");}

*/
        System.out.println(Arrays.toString(numbers));
    }
}






/*class Example {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();

        String[] dataHeight = str.split(" ");
        int[] numbers = new int [dataHeight.length];
        for (int i=0; i<dataHeight.length; i++) {
            int nums  = dataHeight[i];}
        //Arrays.sort (numbers);
        System.out.println(numbers);

    }
}*/


class MyNumbr {
    public static void main(String[] args) {
        int[] array = {3,6,2,14,1};

        Arrays.sort(array);
        System.out.println(Arrays.toString(array));

    }
}
/*String []arr=new int[5];

не нужна. Читаем в массив строк со сканнера с использованием split (пункт 1):

String [] num = scanner.nextLine().split(" ")
Потом пункт 2:

 int [] aq=new int[num.length];

Потом цикл (пункт 3):

arr[i]=sc.next(); - не нужна

 int [] aq=Integer.parseInt(num[i]);

А пункт 4 у вас как раз правильно )*/




class Mann {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        String line = sc.nextLine();
        String [] y = line.split(" ");
        System.out.print(y[0]);

        for (int i = 1; i<y.length-2; i+=2) {
            System.out.print(y[i+1]+" "+y[i]+" ");
        }

        if (y.length%2==0) {
            System.out.print(y[y.length-1]);
        }
        else {
            System.out.print(y[y.length-2] + " " + y[y.length-1]);
        }

    }
}


class Sortirovka {
    public static void main (String[] args) {
        Scanner sc = new Scanner (System.in);
        String stroka = sc.nextLine();
        String [] y = stroka.split(" ");
        int b = stroka.length();
        int numbers [] = new int[b];
        for (int i = 0; i < b; i++ ) {
                numbers [i] = Integer.parseInt(y[i]);

        }
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
    }
}

/*mport java.util.Scanner;
import java.util.Arrays;

class Example {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String str = scanner.nextLine();
        String[] strArr = str.split(" ");
        int[] arr = new int[strArr.length];

        for(int i = 0; i < strArr.length; i++) {
            arr[i] = Integer.parseInt(strArr[i]);
        }

        Arrays.sort(arr);

 */

 /*class MySolution {
    public static void main(String[] args) {
        String[] arrayStr = new Scanner(System.in).nextLine().split(" ");
        int x = 0;
        int y = arrayStr.length[0];
        int[] arr = new int[arrayStr.length];
        for (int i =1; i<arrayStr.length(); i++) {
            if (arr[i].length < arr[0].length) {
                x+=1;}
        System.out.println (arr[x-1]);}

    }
}

class Myr {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String [] a=sc.nextLine().split(" ");
        int leng=a[0].length();
        int min=2;
        for (int i=0; i<a.length; i++){
            if(a[i].length()<min ) {
                min=i+1;}}
        System.out.print(min);
    }


    }

*/
class pifagor {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        for (int i = 1; i<=n; i++) {
            for (int j = 1; j<=m; j++) {
                System.out.print(i*j + " ");}
            System.out.println();}



    }}



class Example {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String y = scanner.nextLine();
        int m = Integer.parseInt (y);
        String str = scanner.nextLine();
        String[] strArr = str.split(" ");
        int[] arr = new int[strArr.length];

        for(int i = 0; i < strArr.length; i++) {
            arr[i] = Integer.parseInt(strArr[i]);}
        if (m<0 || m>arr.length-1) {
            System.out.println ("Неверный ввод");
        }
        else {
            System.out.println (arr[m]);
        }

    }
}


    class Exampleee {
public static void main(String[] args) {double[] array = { 0.22, 0.4, 0.92, 1.5, 1.99, 4.5 };
    int indexOfMax = 0;
    int indexOfMin = 0;
for (int i = 1; i < array.length; i++)
        {
        if (array[i] > array[indexOfMax])
        {
        indexOfMax = i;
        }
        else if (array[i] < array[indexOfMin])
        {
        indexOfMin = i;
        }
        }
        System.out.println(indexOfMax + " " + indexOfMin);}}




        class dddd{
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);

            System.out.println(scanner.nextInt());
            System.out.println(scanner.nextLine());
        }}



    class Soltion {
    public static void main(String[] args) {
        System.out.println("Фокус-покус! Введите две строки!!");
        Scanner scan = new Scanner(System.in);
        String t1 = scan.nextLine();
        System.out.println("Вы ввели строку: " + t1);
        String t2 = scan.nextLine();
        System.out.println("Вы ввели строку: " + t2);
        System.out.println("Первая строка: " + t1 + ", а вторая: " + t2);

        System.out.println("Теперь введите число, а потом строку!!");
        if (scan.hasNextInt()) {
            int z1 = scan.nextInt();
            System.out.println("Вы ввели число: " + z1 + ", теперь вводите строку!!");
        }
        else
            System.out.println("Вы ввели строку. Так не честно :(");

        String t3 = scan.nextLine();
        System.out.println("ТУТ ДОЛЖНА БЫТЬ СТРОЧКА. ГДЕ СТРОЧКА?!");

        String t4 = scan.nextLine();
        System.out.println("четвертую строчку мы увидели: " + t4);
    }
}


/*

class MySolution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String[] Arr = sc.nextLine().split(" ");
                }
        for (int i = 0; i<Arr.length; int [] arr2 = Integer.Parseint(Arr);
    }
}




class MySotion {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 0;
        while(sc.hasNext()){
            String[] arr0 = sc.nextLine().split(" ");}
        int [] arr1 = Integer.parseInt(arr0);
        int x1 = arr1.length;
        int x2 = arr1[0].length;
        int [][] arr2 = new int [x2][x1];
        for (int i = 0; i < x1; ++i){
            for (int j = 0; j < x2; ++j) {           // эквивалент так тоже можно
                arr2[j][i] = arr1[x1 - i - 1][j];  }
        }
        for (int i = 0; i < x2; ++i)
        {
            for (int j = 0; j < x1; ++j)

                str += arr2[i][j] + " ";                   // наполняем строку
            str = str.substring(0, str.length() - 1);  // вырезаем пробел в конце!
            System.out.println(str);
            str = "";}
    }

}

*/






class MySion {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int counter = 0, k = 0;
        String str = "";
        while (sc.hasNext()) {
            String input = sc.nextLine();
            str += input + " ";
            counter++;
        }
        String[] arr1 = str.split(" ");
        String[][] matrix = new String[counter][arr1.length / counter];
        int x1 = arr1.length;
        int x2 = arr1[0].length();
        int [][] arr2 = new int [x2][x1];
        for (int i = 0; i < x2; ++i)
            for (int j = 0; j < x1; ++j)        // эквивалент так тоже можно
                arr2[j][i] = arr1[i][x1-j];
        for (int i = 0; i < x2; ++i)
        {
            for (int j = 0; j < x1; ++j)

                str += arr2[i][j] + " ";                   // наполняем строку
            str = str.substring(0, str.length() - 1);  // вырезаем пробел в конце!
            System.out.println(str);
            str = "";
        }
    }
}



